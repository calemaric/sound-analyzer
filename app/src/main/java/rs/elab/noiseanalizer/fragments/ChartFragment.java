package rs.elab.noiseanalizer.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import rs.elab.noiseanalizer.R;
import rs.elab.noiseanalizer.SoundCaptureActivity;
import rs.elab.noiseanalizer.threads.AudioRecordThread;

public class ChartFragment extends Fragment implements AudioRecordThread.OnAudioRecordingInterface {
    private OnFragmentInteractionListener mListener;

    private LineChart soundChart = null;
    private List<Entry> chartDataEntries = null;

    private Button startRecordingButton = null;
    private Button stopRecordingButton = null;

    private TextView timeView = null;

    private AudioRecordThread audioRecordThread = null;
    int totalSeconds = 0;
    private Timer timer = null;
    private int numOfSample = 0;

    private HashMap<String, Double> signalMap = new HashMap<>();

    public ChartFragment() {
        // Required empty public constructor
    }

    public static ChartFragment newInstance() {
        ChartFragment fragment = new ChartFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onResume() {
        super.onResume();
        setUpChart();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (audioRecordThread != null && audioRecordThread.isAlive()) {
            audioRecordThread.interrupt();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chart, container, false);
        soundChart = (LineChart) view.findViewById(R.id.sound_chart);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        chartDataEntries = new ArrayList<>();

        startRecordingButton = (Button) view.findViewById(R.id.start_recording_button);
        stopRecordingButton = (Button) view.findViewById(R.id.stop_recording_button);

        timeView = (TextView) view.findViewById(R.id.time_view);
        timeView.setText(String.valueOf(0));

        startRecordingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRecording();
            }
        });

        stopRecordingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private void setUpChart() {
        soundChart.getXAxis().setAxisMinimum(0);
        soundChart.getAxis(YAxis.AxisDependency.LEFT).setAxisMinimum(0);
        soundChart.getAxis(YAxis.AxisDependency.LEFT).setAxisMaximum(100);
        soundChart.getAxis(YAxis.AxisDependency.RIGHT).setEnabled(false);
        soundChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        soundChart.getXAxis().setDrawGridLines(false);
        soundChart.getXAxis().setAxisMinimum(0);
        soundChart.getXAxis().setAxisMaximum(100);
        chartDataEntries.add(new Entry(0, 0));
        LineDataSet lineDataSet = new LineDataSet(chartDataEntries, "");
        LineData lineData = new LineData(lineDataSet);
        soundChart.setData(lineData);
        soundChart.invalidate();
    }

    public void startRecording() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    SoundCaptureActivity.MY_PERMISSIONS_REQUEST_RECORD_AUDIO);

        } else {
            audioRecordThread = new AudioRecordThread(this);
            audioRecordThread.start();
            totalSeconds = 0;
            timer = new Timer();
            numOfSample = 0;

            signalMap = new HashMap<>();

            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            timeView.setText(String.valueOf(totalSeconds++));
                        }
                    });
                }
            }, 0, 1000L);

            startRecordingButton.setVisibility(View.GONE);
            stopRecordingButton.setVisibility(View.VISIBLE);
        }
    }

    public void stopRecording() {
        if (audioRecordThread != null) {
            audioRecordThread.stopRecording();
            startRecordingButton.setVisibility(View.VISIBLE);
            stopRecordingButton.setVisibility(View.GONE);
        }

        if (timer != null) {
            timer.cancel();
        }

        for (Map.Entry<String, Double> entry : signalMap.entrySet()) {
            entry.setValue(entry.getValue() / numOfSample);
        }

        mListener.onDataCollected(signalMap);
    }

    @Override
    public void readAudioDataArray(final double[] data, final int rate) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chartDataEntries.clear();
                chartDataEntries.add(new Entry(0, 0));
                for (int i = 0; i < data.length; i++) {
                    int x = rate * i;

                    int downy = (int) ((data[i] * 10));
                    chartDataEntries.add(new Entry(x, downy));
                }

                LineDataSet lineDataSet = new LineDataSet(chartDataEntries, "");
                soundChart.getXAxis().setAxisMaximum(data.length * rate);
                soundChart.setData(new LineData(lineDataSet));
                soundChart.invalidate();
            }
        });

        calculateAverageSignal(data, rate);
        numOfSample++;
    }

    public void calculateAverageSignal(double[] data, int rate) {
        for (int i = 0; i < data.length / 2; i++) {
            double x = rate * i;

            double re = data[2 * i];
            double im = data[2 * i + 1];

            double res = Math.sqrt(re * re + im * im);

            if (!signalMap.containsKey(x + "")) {
                signalMap.put(x + "", res);
            } else {
                signalMap.put(x + "", signalMap.get(x + "") + res);
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        stopRecording();
    }

    public interface OnFragmentInteractionListener {
        void onDataCollected(HashMap<String, Double> signalMap);
    }
}
