package rs.elab.noiseanalizer.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.util.Log;

import rs.elab.noiseanalizer.R;

public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        // Load the Preferences from the XML file
        addPreferencesFromResource(R.xml.app_preferences);
        EditTextPreference editTextPreference = (EditTextPreference) findPreference("url");

        editTextPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Log.d("url", newValue.toString());
                SharedPreferences.Editor editor = getActivity().getSharedPreferences("settings", Context.MODE_APPEND).edit();
                editor.putString("url", newValue.toString());
                editor.apply();

                return false;
            }
        });
    }
}