package rs.elab.noiseanalizer;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.AudioRecord;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import rs.elab.noiseanalizer.fragments.ChartFragment;
import rs.elab.noiseanalizer.fragments.MapFragment;
import rs.elab.noiseanalizer.fragments.SettingsFragment;

public class SoundCaptureActivity extends AppCompatActivity implements MapFragment.OnFragmentInteractionListener, ChartFragment.OnFragmentInteractionListener {
    public static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 2;

    private static final String RECORD_FRAGMENT = "record";
    private static final String MAP_FRAGMENT = "map";
    private static final String SETTINGS_FRAGMENT = "settings";

    private Fragment fragmentContainer = null;
    private DrawerLayout mDrawerLayout = null;
    private ActionBarDrawerToggle mDrawerLayoutToggle = null;
    private NavigationView navigationView = null;

    private LatLng currentLocation = null;
    private HashMap<String, Double> signalMap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound_capture);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigationView = (NavigationView) findViewById(R.id.nvView);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_record_fragment:
                        Log.d("nav", "record");
                        handleOnNavigationItemSelected(RECORD_FRAGMENT);
                        return true;
                    case R.id.nav_map_fragment:
                        Log.d("nav", "record");
                        handleOnNavigationItemSelected(MAP_FRAGMENT);
                        return true;
                    case R.id.nav_settings_fragment:
                        Log.d("nav", "record");
                        handleOnNavigationItemSelected(SETTINGS_FRAGMENT);
                        return true;
                }
                return false;
            }
        });

        handleOnNavigationItemSelected(MAP_FRAGMENT);
    }

    private void handleOnNavigationItemSelected(String fragment) {
        int fragmentLastIndex = getSupportFragmentManager().getBackStackEntryCount() - 1;
        Fragment fragmentView;

        mDrawerLayout.closeDrawer(Gravity.START);

        switch (fragment) {
            case MAP_FRAGMENT:
                fragmentView = MapFragment.newInstance();
                break;
            case RECORD_FRAGMENT:
                fragmentView = ChartFragment.newInstance();
                break;
            case SETTINGS_FRAGMENT:
                fragmentView = new SettingsFragment();
                break;
            default:
                return;
        }

        if (fragmentLastIndex > -1) {
            String fragmentName = getSupportFragmentManager()
                    .getBackStackEntryAt(fragmentLastIndex)
                    .getName();

            if (fragmentName.equals(fragment)) {
                return;
            }
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragmentView)
                .addToBackStack(fragment)
                .commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.nav_record_fragment:
                Log.d("nav", "first");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
            }
            break;
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Fragment mapFragment = MapFragment.newInstance();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_frame, mapFragment)
                            .addToBackStack(MAP_FRAGMENT)
                            .commit();
                }
            }
        }
    }

    @Override
    public void onLocationChange(LatLng latLng) {
        currentLocation = latLng;
    }

    @Override
    public void onDataCollected(HashMap<String, Double> signalMap) {
        SharedPreferences sharedPreferences = getSharedPreferences("settings", MODE_APPEND);

        this.signalMap = signalMap;
        UploadStatsTask uploadStatsTask = new UploadStatsTask();
        uploadStatsTask.execute(sharedPreferences.getString("url", ""));
    }

    private class UploadStatsTask extends AsyncTask<String, Integer, Long> {
        protected Long doInBackground(String... urls) {
            try {
                Log.d("request url", urls[0]);
                URL url = new URL(urls[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Content-Type","application/json");

                try {
                    DataOutputStream out = new DataOutputStream(urlConnection.getOutputStream());

                    JSONObject request = new JSONObject();


                    if (currentLocation != null) {
                        try {
                            request.put("lon", currentLocation.longitude);
                            request.put("lat", currentLocation.latitude);
                            request.put("analiza", new JSONObject(signalMap));
                            request.put("data", "");
                            request.put("naziv", "test");
                            request.put("opis", "opis");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    Log.d("request url", urls[0]);

                    Log.d("request", request.toString());
                    out.write(request.toString().getBytes());
                    out.flush();

                    int responseCode = urlConnection.getResponseCode();

                    if(responseCode == HttpURLConnection.HTTP_OK) {
                        return 1L;
                    } else {
                        return 0L;
                    }
                } finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            return 0L;
        }

        protected void onPostExecute(Long result) {
            if(result == 1L) {
                //Toast.makeText(SoundCaptureActivity.this, "Success", Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(SoundCaptureActivity.this, "Fail", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
